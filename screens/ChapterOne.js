import React from 'react';
import { View, Text, Image, StyleSheet, Dimensions } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { tutorialSpec } from '../config/theme';

const { RADIUS, SPACING, FULL_SIZE } = tutorialSpec;
import { SharedElement } from 'react-navigation-shared-element';

import * as Animatable from 'react-native-animatable';
import { FlatList } from 'react-native-gesture-handler';
import BottomSheet, { BottomSheetScrollView } from '@gorhom/bottom-sheet';
import { BaseButton, GestureHandlerRootView } from 'react-native-gesture-handler';

import chapterOne from '../images/chapterOne.svg';
import { SvgUri } from 'react-native-svg';
import { Audio } from 'expo-av';
import { AntDesign, Ionicons } from '@expo/vector-icons';
const { width, height } = Dimensions.get('screen');

const ITEM_HEIGHT = height * 0.75;
const ITEM_WIDTH = width;

const ChapterOne = ({ navigation, route }) => {
	const [sound, setSound] = React.useState();

	const [control, setControl] = React.useState('play');

	async function playSound() {
		console.log('Loading Sound');
		// const { sound } = await Audio.Sound.createAsync( require('./assets/Hello.mp3')
		// );

		const sound = new Audio.Sound();

		await sound.loadAsync({
			uri: 'https://s3.ap-south-1.amazonaws.com/squapl.com/mouse.mp3',
		});

		await sound.playAsync();

		setSound(sound);

		console.log('Playing Sound');
		await sound.playAsync();
	}
	async function togglePlay() {
		if (control === 'play') {
			setControl('pause');
		} else {
			setControl('play');
		}
	}

	let button;

	React.useEffect(() => {
		if (control === 'play') {
			playSound();

			button = (
				<AntDesign
					name='pause'
					size={24}
					color='#333'
					style={{
						paddingHorizontal: SPACING,
						position: 'absolute',
						bottom: 50,
						right: 50,
						zIndex: 2,
					}}
					onPress={() => this.togglePlay}
				/>
			);
		} else if (control === 'pause') {
			sound.stopAsync();

			button = (
				<AntDesign
					name='play'
					size={42}
					color='#333'
					style={{
						paddingHorizontal: SPACING,
						position: 'absolute',
						bottom: 20,
						right: 20,
						zIndex: 2,
					}}
					onPress={togglePlay}
				/>
			);
		}
	}, [control]);

	return (
		<View style={{ flex: 1 }}>
			<AntDesign
				name='arrowleft'
				size={42}
				color='#333'
				style={{
					paddingHorizontal: SPACING,
					position: 'absolute',
					top: 50,
					left: 10,
					zIndex: 2,
				}}
				onPress={() => navigation.goBack()}
			/>

			<AntDesign
				name='arrowright'
				size={42}
				color='#333'
				style={{
					paddingHorizontal: SPACING,
					position: 'absolute',
					top: 50,
					right: 10,
					zIndex: 2,
				}}
				onPress={() => {
					navigation.push('ChapterTwo');
				}}
			/>

			<SharedElement id={`1`} style={[StyleSheet.absoluteFillObject]}>
				<View style={[StyleSheet.absoluteFillObject, { borderRadius: 0 }]}>
					<Image
						source={{ uri: `https://s3.ap-south-1.amazonaws.com/squapl.com/story-11.png` }}
						style={[StyleSheet.absoluteFillObject, { resizeMode: 'cover' }]}
					/>

					{/* <SvgUri
						width='100%'
						height='100%'
						uri='https://s3.ap-south-1.amazonaws.com/squapl.com/story-11.png'
					/> */}
				</View>
			</SharedElement>

			<BottomSheet initialSnapIndex={0} snapPoints={['15%', height - ITEM_HEIGHT, height]}>
				<GestureHandlerRootView style={{ flex: 1 }}>
					{control === 'play' ? (
						<Ionicons
							name='ios-pause-circle-sharp'
							size={50}
							color='#333'
							style={{
								paddingHorizontal: SPACING,
								position: 'absolute',
								top: 0,
								right: 30,
								zIndex: 2,
							}}
							onPress={togglePlay}
						/>
					) : (
						<Ionicons
							name='ios-play-circle-sharp'
							size={50}
							color='#333'
							style={{
								paddingHorizontal: SPACING,
								position: 'absolute',
								top: 0,
								right: 20,
								zIndex: 2,
							}}
							onPress={togglePlay}
						/>
					)}
					<BottomSheetScrollView style={{ backgroundColor: 'white' }} contentContainerStyle={{ padding: 20 }}>
						<View style={{ fontWeight: 'bold' }}>
							<Text style={{ fontSize: 16, fontWeight: 'bold' }}>Mouse :</Text>
						</View>
						<View style={{ marginVertical: 16 }}>
							<Text style={{ marginBottom: 10, lineHeight: 22 }}>
								Mystic mouse had always been curious about life but was never happy with people’s
								answers. He heard from his buerfl y friend about Lion Sage and decided to seek him out.
							</Text>
						</View>
						<View style={{ fontWeight: 'bold' }}>
							<Text style={{ fontSize: 16, fontWeight: 'bold' }}>Lion :</Text>
						</View>
						<View style={{ marginVertical: 16 }}>
							<Text style={{ marginBottom: 10, lineHeight: 22 }}>
								Well, little mouse, allow me to welcome you on the Great Path of self- discovery. On
								this path you will find the answers to your many questions, and answers to questions you
								don’t even have yet. I have a question for you: Are you ready?
							</Text>
						</View>
					</BottomSheetScrollView>
				</GestureHandlerRootView>
			</BottomSheet>
		</View>
	);
};

const styles = StyleSheet.create({
	header: {
		height: 60,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: '#008080',
	},
	headerText: {
		color: 'white',
		fontWeight: 'bold',
		fontSize: 18,
	},
	container: {
		flex: 1,
		padding: 16,
	},
	location: {
		fontSize: 30,
		color: 'white',
		fontWeight: '800',
		width: ITEM_WIDTH * 0.8,
		textTransform: 'uppercase',
		position: 'absolute',
		top: 100,
		left: SPACING * 2,
	},
});

export default ChapterOne;
