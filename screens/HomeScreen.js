import React from 'react';

import { SafeAreaView } from 'react-native-safe-area-context';
import { StyleSheet, Text, View, Image, TouchableOpacity, Animated, Button } from 'react-native';
import { StatusBar } from 'expo-status-bar';

import Svg, { Circle, Path } from 'react-native-svg';

import Lion from '../images/lion.svg';
import data from '../data/locations';
import { tutorialSpec } from '../config/theme';

import ChapterDetails from '../screens/ChapterDetails';
import { SharedElement } from 'react-navigation-shared-element';
import { FlatList } from 'react-native-gesture-handler';
import SVGatorComponent from '../components/SVGatorComponent';
import { SvgUri } from 'react-native-svg';
import { Audio } from 'expo-av';
import { AntDesign, Ionicons } from '@expo/vector-icons';

const { ITEM_WIDTH, ITEM_HEIGHT, RADIUS, SPACING, FULL_SIZE } = tutorialSpec;

function HomeScreen({ navigation }) {
	const scrollx = React.useRef(new Animated.Value(0)).current;
	const [sound, setSound] = React.useState();

	const [control, setControl] = React.useState('play');

	async function playSound() {
		console.log('Loading Sound');
		const { sound } = await Audio.Sound.createAsync(require('../assets/om-nama-shivaya.mp3'));

		//	const sound = new Audio.Sound();

		// const { sound } = await Audio.Sound.loadAsync({
		// 	uri: 'https://s3.ap-south-1.amazonaws.com/squapl.com/om-nama-shivaya.mp3',
		// });

		//await sound.playAsync();

		setSound(sound);

		console.log('Playing Sound');
		await sound.playAsync();
	}

	async function togglePlay() {
		if (control === 'play') {
			setControl('pause');
		} else {
			setControl('play');
		}
	}

	let button;

	React.useEffect(() => {
		if (control === 'play') {
			playSound();

			button = (
				<AntDesign
					name='pause'
					size={24}
					color='#333'
					style={{
						paddingHorizontal: SPACING,
						position: 'absolute',
						bottom: 50,
						right: 50,
						zIndex: 2,
					}}
					onPress={() => this.togglePlay}
				/>
			);
		} else if (control === 'pause') {
			sound.stopAsync();

			button = (
				<AntDesign
					name='play'
					size={42}
					color='#333'
					style={{
						paddingHorizontal: SPACING,
						position: 'absolute',
						bottom: 20,
						right: 20,
						zIndex: 2,
					}}
					onPress={togglePlay}
				/>
			);
		}
	}, [control]);

	return (
		<>
			<SafeAreaView>
				{/* DO NOT DELETE */}

				{/* 
            <SVGatorComponent />
            <Svg viewBox='0 0 50 50' fill='blue'>
    			<Circle cx='50%' cy='50%' r='40%' fill='pink' />
    			<Path d='M 9.15625 6.3125 L 6.3125 9.15625 L 22.15625 25 L 6.21875 40.96875 L 9.03125 43.78125 L 25 27.84375 L 40.9375 43.78125 L 43.78125 40.9375 L 27.84375 25 L 43.6875 9.15625 L 40.84375 6.3125 L 25 22.15625 Z' />
    		</Svg> 

			 <Image style={styles.backgroundImage} source={require('../images/ic.gif')} />  

			 <Image source={require('../images/ic.gif')} style={{ width: 50, height: 50 }} /> 

			<View> 
				<Lion />
			</View>

            <Text style={{wordWrap: 'break-word'}}>
            */}

				<View style={styles.header}>
					<Text style={styles.headerText}>Chapters</Text>
				</View>
				<Animated.FlatList
					data={data}
					keyExtractor={(item) => item.key}
					horizontal
					showsHorizontalScrollIndicator={false}
					snapToInterval={FULL_SIZE}
					decelerationRate='fast'
					onScroll={Animated.event([{ nativeEvent: { contentOffset: { x: scrollx } } }], {
						useNativeDriver: true,
					})}
					renderItem={({ item, index }) => {
						const inputRange = [(index - 1) * FULL_SIZE, index * FULL_SIZE, (index + 1) * FULL_SIZE];
						const translatex = scrollx.interpolate({
							inputRange,
							outputRange: [ITEM_WIDTH, 0, -ITEM_WIDTH],
						});

						const scale = scrollx.interpolate({
							inputRange,
							outputRange: [1, 1.1, 1],
						});
						return (
							<>
								<TouchableOpacity
									onPress={() => {
										navigation.push('ChapterDetails', { item });
									}}>
									<View style={styles.itemContainer}>
										<SharedElement
											id={`item.${item.key}.photo`}
											style={[StyleSheet.absoluteFillObject]}>
											<View
												style={[
													StyleSheet.absoluteFillObject,
													{ overflow: 'hidden', borderRadius: RADIUS },
												]}>
												<Animated.Image
													// source={{
													// 	uri: 'https://s3.ap-south-1.amazonaws.com/squapl.com/chapterOne.png',
													// }}
													source={{ uri: item.image }}
													style={[
														StyleSheet.absoluteFillObject,
														{ resizeMode: 'cover', transform: [{ scale }] },
													]}
												/>
											</View>
										</SharedElement>
										<SharedElement id={`item.${item.key}.location`}>
											<Animated.Text
												style={[
													styles.location,
													{
														transform: [{ translateX: translatex }],
													},
												]}>
												{item.location}
											</Animated.Text>
										</SharedElement>

										<View style={styles.days}>
											<Text style={styles.daysValue}>{item.numberOfDays}</Text>

											<Text style={styles.daysLabel}>pages</Text>
										</View>
									</View>
									<View style={styles.descriptionSection}>
										<Text style={styles.toc}>{item.toc}</Text>
										<Text style={styles.descHead}>{item.location}</Text>
										<Text numberOfLines={2} ellipsizeMode='tail' style={styles.description}>
											{item.description}
										</Text>
										<Text style={{ textAlign: 'center' }}>
											<SvgUri width='200' height='200' uri={item.cartoon} />
										</Text>
									</View>
								</TouchableOpacity>
							</>
						);
					}}
				/>
				{control === 'play' ? (
					<Ionicons
						name='ios-pause-circle-sharp'
						size={60}
						color='#333'
						style={{
							paddingHorizontal: SPACING,
							position: 'absolute',
							bottom: 50,
							right: 30,
							zIndex: 2,
						}}
						onPress={togglePlay}
					/>
				) : (
					<Ionicons
						name='ios-play-circle-sharp'
						size={60}
						color='#333'
						style={{
							paddingHorizontal: SPACING,
							position: 'absolute',
							bottom: 50,
							right: 20,
							zIndex: 2,
						}}
						onPress={togglePlay}
					/>
				)}
			</SafeAreaView>
		</>
	);
}

export default HomeScreen;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'center',
	},

	itemContainer: {
		width: ITEM_WIDTH,
		height: ITEM_HEIGHT,
		margin: SPACING,
	},

	location: {
		fontSize: 30,
		color: 'white',
		fontWeight: '800',
		width: ITEM_WIDTH * 0.8,
		textTransform: 'uppercase',
		position: 'absolute',
		top: SPACING,
		left: SPACING,
	},
	days: {
		position: 'absolute',
		bottom: SPACING,
		left: SPACING,
		width: 52,
		height: 52,
		borderRadius: 26,
		backgroundColor: 'tomato',
		justifyContent: 'center',
		alignItems: 'center',
	},
	daysValue: { fontWeight: '800', fontSize: 18, color: 'white' },
	daysLabel: {
		color: 'white',
		fontSize: 10,
	},

	headerText: {
		fontSize: 24,
		fontWeight: '800',
		color: '#905714',
		textAlign: 'left',
	},
	header: {
		paddingLeft: 10,
		paddingTop: 10,
		paddingBottom: 0,
	},
	descriptionSection: {
		padding: 20,
		paddingTop: 10,
		width: ITEM_WIDTH,
	},
	toc: {
		fontSize: 13,
		color: '#905714',
		fontWeight: '800',
	},
	descHead: {
		fontSize: 19,
		fontWeight: '600',
		color: '#905714',
	},
	description: {
		fontSize: 18,
	},
});
