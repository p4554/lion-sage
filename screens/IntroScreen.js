import React from 'react';

import { SafeAreaView } from 'react-native-safe-area-context';
import { StyleSheet, Text, View, Image, TouchableOpacity, Animated } from 'react-native';
import { StatusBar } from 'expo-status-bar';

import Svg, { Circle, Path } from 'react-native-svg';

import Intro from '../images/intro.svg';
import data from '../data/locations';
import GetStarted from '../images/get-started.svg';

import { tutorialSpec } from '../config/theme';

import ChapterDetails from './ChapterDetails';
import { SharedElement } from 'react-navigation-shared-element';
import { FlatList } from 'react-native-gesture-handler';
import SVGatorComponent from '../components/SVGatorComponent';

const { ITEM_WIDTH, ITEM_HEIGHT, RADIUS, SPACING, FULL_SIZE } = tutorialSpec;

function IntroScreen({ navigation }) {
	const scrollx = React.useRef(new Animated.Value(0)).current;
	return (
		<>
			<View style={{ display: 'flex', justifyContent: 'center', padding: SPACING * 2, marginTop: SPACING * 2 }}>
				<View style={{ justifyContent: 'center', alignItems: 'center' }}>
					<Intro />
				</View>

				<Text
					style={{
						fontSize: 40,
						fontWeight: 'medium',
						textAlign: 'center',
						paddingTop: SPACING * 2,
					}}>
					Lion Sage
				</Text>
				<Text
					style={{
						fontSize: 24,
						fontWeight: 'medium',
						textAlign: 'center',
					}}>
					Merging with Siva for Kids
				</Text>

				<Text
					style={{
						fontSize: 18,
						fontWeight: 'medium',
						textAlign: 'left',
						paddingTop: SPACING * 3,
					}}>
					SATGURU SIVAYA SUBRAMUNIYASWAMI
				</Text>

				{/* DO NOT DELETE */}

				{/* 
            <SVGatorComponent />
            <Svg viewBox='0 0 50 50' fill='blue'>
    			<Circle cx='50%' cy='50%' r='40%' fill='pink' />
    			<Path d='M 9.15625 6.3125 L 6.3125 9.15625 L 22.15625 25 L 6.21875 40.96875 L 9.03125 43.78125 L 25 27.84375 L 40.9375 43.78125 L 43.78125 40.9375 L 27.84375 25 L 43.6875 9.15625 L 40.84375 6.3125 L 25 22.15625 Z' />
    		</Svg> 

			 <Image style={styles.backgroundImage} source={require('../images/ic.gif')} />  

			 <Image source={require('../images/ic.gif')} style={{ width: 50, height: 50 }} /> 

			<View> 
				<Lion />
			</View>
            */}
			</View>

			<View style={{ position: 'absolute', bottom: 30, right: 40 }}>
				<TouchableOpacity
					onPress={() => {
						navigation.push('Home');
					}}>
					<GetStarted />
				</TouchableOpacity>
			</View>
		</>
	);
}

export default IntroScreen;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'center',
	},

	itemContainer: {
		width: ITEM_WIDTH,
		height: ITEM_HEIGHT,
		margin: SPACING,
	},

	location: {
		fontSize: 30,
		color: 'white',
		fontWeight: '800',
		width: ITEM_WIDTH * 0.8,
		textTransform: 'uppercase',
		position: 'absolute',
		top: SPACING,
		left: SPACING,
	},
	days: {
		position: 'absolute',
		bottom: SPACING,
		left: SPACING,
		width: 52,
		height: 52,
		borderRadius: 26,
		backgroundColor: 'tomato',
		justifyContent: 'center',
		alignItems: 'center',
	},
	daysValue: { fontWeight: '800', fontSize: 18, color: 'white' },
	daysLabel: {
		color: 'white',
		fontSize: 10,
	},
});
