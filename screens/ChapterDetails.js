import React from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { tutorialSpec, width } from '../config/theme';
import { AntDesign } from '@expo/vector-icons';
const { ITEM_WIDTH, ITEM_HEIGHT, RADIUS, SPACING, FULL_SIZE } = tutorialSpec;
import { SharedElement } from 'react-navigation-shared-element';

import * as Animatable from 'react-native-animatable';
import { FlatList } from 'react-native-gesture-handler';

const zoomIn = {
	0: {
		opacity: 0,
		scale: 0.5,
	},
	0.5: {
		opacity: 1,
		scale: 1.2,
	},
	1: {
		opacity: 1,
		scale: 1,
	},
};

const ChapterDetails = ({ navigation, route }) => {
	const { item } = route.params;

	return (
		<SafeAreaView style={{ flex: 1 }}>
			<AntDesign
				name='arrowleft'
				size={24}
				color='#333'
				style={{
					paddingHorizontal: SPACING,
					position: 'absolute',
					top: 50,
					left: 10,
					zIndex: 2,
				}}
				onPress={() => navigation.goBack()}
			/>
			<SharedElement id={`item.${item.key}.photo`} style={[StyleSheet.absoluteFillObject]}>
				<View style={[StyleSheet.absoluteFillObject, { borderRadius: 0 }]}>
					<Image
						source={{ uri: item.image }}
						style={[StyleSheet.absoluteFillObject, { resizeMode: 'cover' }]}
					/>
				</View>
			</SharedElement>
			<SharedElement id={`item.${item.key}.location`}>
				<Text style={[styles.location]}>{item.location}</Text>
			</SharedElement>

			<View style={{ position: 'absolute', bottom: 120 }}>
				{/* <Text
					style={[
						{
							fontSize: 16,
							width: '100%',
							textTransform: 'uppercase',
							fontWeight: '800',
							color: '#fff',
							marginHorizontal: SPACING,
						},
					]}>
					Activities
				</Text> */}

				<FlatList
					data={[...Array(8).keys()]}
					keyExtractor={(item) => item.toString()}
					horizontal
					showsHorizontalScrollIndicator={false}
					contentContainerStyle={{ padding: SPACING }}
					renderItem={({ item, index }) => {
						return (
							<TouchableOpacity
								onPress={() => {
									navigation.push('ChapterOne');
								}}>
								<Animatable.View
									animation={zoomIn}
									duration={700}
									delay={400 + index * 100}
									style={{
										backgroundColor: '#fff',
										padding: SPACING,
										width: width * 0.33,
										height: width * 0.55,
										marginRight: SPACING,
										borderRadius: 12,
									}}>
									<Image
										source={{ uri: 'https://s3.ap-south-1.amazonaws.com/squapl.com/page-1.png' }}
										style={{ width: '100%', height: '90%', resizeMode: 'cover' }}
									/>
									<Text>Page #{item + 1}</Text>
								</Animatable.View>
							</TouchableOpacity>
						);
					}}
				/>
			</View>
		</SafeAreaView>
	);
};

const styles = StyleSheet.create({
	header: {
		height: 60,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: '#008080',
	},
	headerText: {
		color: 'white',
		fontWeight: 'bold',
		fontSize: 18,
	},
	container: {
		flex: 1,
		padding: 16,
	},
	location: {
		fontSize: 30,
		color: 'white',
		fontWeight: '800',
		width: ITEM_WIDTH * 0.8,
		textTransform: 'uppercase',
		position: 'absolute',
		top: 100,
		left: SPACING * 2,
	},
});

ChapterDetails.sharedElements = (route, otherRoute, showing) => {
	const { item } = route.params;
	return [{ id: `item.${item.key}.photo`, location: `item.${item.key}.location` }];

	// return data.map((item) => ({ id: `item.${item.key}.photo`, align: 'center-bottom' }));
};

export default ChapterDetails;
