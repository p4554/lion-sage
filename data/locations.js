export default [
	{
		key: '1',
		location: 'Are You Ready?',
		numberOfDays: 22,
		image: 'https://s3.ap-south-1.amazonaws.com/squapl.com/chapterOne.png',
		color: '#24A6D9',
		description: 'In which Mystic Mouse meets and is challenged by Lion Sage',
		toc: 'Chapter 1',
		cartoon: 'https://s3.ap-south-1.amazonaws.com/squapl.com/final-lion.svg',
	},
	{
		key: '2',
		location: 'Who Are You?',
		numberOfDays: 19,
		image: 'https://s3.ap-south-1.amazonaws.com/squapl.com/chapterTwo.png',
		color: '#CB5578',
		description:
			'in which Mystic Mouse learns he is not his mind, body or emotions, but is an immortal, perfect soul on the path to the Self ',
		toc: 'Chapter 2',
		cartoon: 'https://s3.ap-south-1.amazonaws.com/squapl.com/mr-final.svg',
	},
	{
		key: '3',
		location: 'The Purpose of Life',
		numberOfDays: 9,
		image: 'https://s3.ap-south-1.amazonaws.com/squapl.com/chapterThree.png',
		color: '#DB4BB9',
		description:
			'In which Lion Sage tells little mouse that he was born on Earth for a profound reason, to know his highest, divine and immortal Self',
		toc: 'Chapter 3',
		cartoon: 'https://s3.ap-south-1.amazonaws.com/squapl.com/mr-rat.svg',
	},
];
