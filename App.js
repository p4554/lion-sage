import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import HomeScreen from './screens/HomeScreen';
import IntroScreen from './screens/IntroScreen';
import ChapterDetails from './screens/ChapterDetails';
import ChapterOne from './screens/ChapterOne';
import ChapterTwo from './screens/ChapterTwo';

export default function App() {
	const Stack = createNativeStackNavigator();
	return (
		<NavigationContainer>
			{/* <Text>Open up App.js to start working on your app! lion d</Text>
				<StatusBar style='auto' /> */}

			<Stack.Navigator
				initialRouteName='Intro'
				screenOptions={{
					headerShown: false,
				}}>
				<Stack.Screen name='Intro' component={IntroScreen} />
				<Stack.Screen name='Home' component={HomeScreen} />
				<Stack.Screen name='ChapterDetails' component={ChapterDetails} />
				<Stack.Screen name='ChapterOne' component={ChapterOne} />
				<Stack.Screen name='ChapterTwo' component={ChapterTwo} />
			</Stack.Navigator>
		</NavigationContainer>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'center',
	},
});

// npx expo start --tunnel
